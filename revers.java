import java.util.Scanner;

public class Revers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String sentence = scanner.nextLine();
        String[] wordTab = sentence.split(" ");
        String word = wordTab[wordTab.length - 1];
        char[] bigChar = word.toCharArray();
        bigChar[0] = Character.toUpperCase(bigChar[0]);
        for (int i = 0; i <bigChar.length; i++) {
            System.out.print(bigChar[i]);
        }
        System.out.print(" ");
        for (int i = 1; i < wordTab.length; i++) {
            System.out.print((wordTab[wordTab.length - 1 - i]) + " ");
        }

    }

}
